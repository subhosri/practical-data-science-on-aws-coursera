# Practical Data Science on AWS Coursera



## 1. Analyze Datasets and Train ML Models using AutoML

* **Week 1: Explore the Use Case and Analyze the Dataset**
        - Ingest, explore, and visualize a product review data set for multi-class text classification
* **Week 2: Data Bias and Feature Importance**
        - Determine the most important features in a data set and detect statistical biases.
* **Week 3: Use Automated Machine Learning to train a Text Classifier**
        - Inspect and compare models generated with automated machine learning (AutoML).
* **Week 4: Built-in algorithms**
        - Train a text classifier with BlazingText and deploy the classifier as a real-time inference endpoint to serve      predictions.

## 2. Build, Train, and Deploy ML Pipelines using BERT

* **Week 1: Feature Engineering and Feature Store**
        - Transform a raw text dataset into machine learning features and store features in a feature store. 
* **Week 2: Train, Debug, and Profile a Machine Learning Model**
        - Fine-tune, debug, and profile a pre-trained BERT model.
* **Week 3: Deploy End-To-End Machine Learning pipelines**
        - Orchestrate ML workflows and track model lineage and artifacts in an end-to-end machine learning pipeline.


## 3. Optimize ML Models and Deploy Human-in-the-Loop Pipelines

* **Week 1: Advanced model training, tuning and evaluation**
        - Train, tune, and evaluate models using data-parallel and model-parallel strategies and automatic model tuning.
* **Week 2: Advanced model deployment and monitoring**
        - Deploy models with A/B testing, monitor model performance, and detect drift from baseline metrics.
* **Week 3: Data labeling and human-in-the-loop pipelines**
        - Label data at scale using private human workforces and build human-in-the-loop pipelines.
